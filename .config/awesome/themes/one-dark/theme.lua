-------------------------------------------------------------------------
--                        MY ONDE DARK AWESOME THEME                   --
-------------------------------------------------------------------------

local gears = require("gears")
local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local naughty = require("naughty")

local dpi = xresources.apply_dpi
local theme_path = gears.filesystem.get_configuration_dir() .. "themes/one-dark/"
local theme = {}

-- Wallpaper
theme.wallpaper = theme_path.."wallpaper.jpg"

-- Fonts
theme.font_name = "Iosevka Nerd Font"

theme.font = "Iosevka Nerd Font 10"
theme.taglist_font = "FiraMono Nerd Font Mono 25"
theme.icon_font = "Iosevka Nerd Font 12"
theme.exit_screen_font = "FiraMono Nerd Font Mono 120"
theme.widget_font = 'Iosevka Nerd Font 10'
theme.notification_font = "Iosevka Nerd Font 10"
theme.tasklist_font = "Iosevka Nerd Font 10"

-- Colors
theme.clr = {
  purple = "#b782da",
  blue = "#729aef",
  green = "#7cd380",
  red = '#ff5370',
  gray = '#97a6c0',
  yellow = '#ffcb6b'
}

theme.bg_normal = '#222632'
theme.bg_focus = '#222632'
--theme.bg_focus = '#545277'
theme.bg_urgent = "#182228"
theme.bg_light = '#2f2f3f'
theme.bg_systray = theme.bg_normal
theme.systray_icon_spacing = dpi(10)

theme.fg_normal = '#525770'
theme.fg_dark = '#424750'
theme.fg_focus = '#b6bcdd'
theme.fg_urgent = '#525770'

theme.tasklist_bg_normal = '#282838'
theme.tasklist_bg_focus = '#2f2f3f'
theme.tasklist_bg_urgent = '#2b2b3b'

theme.prompt_fg = '#868cad'

theme.taglist_bg_focus = theme.bg_light
theme.taglist_fg_occupied = theme.clr.blue
theme.taglist_fg_urgent = theme.clr.red
theme.taglist_fg_empty = theme.clr.gray
theme.taglist_fg_focus = theme.clr.green

theme.notification_fg = '#a6accd'
theme.notification_bg = '#222632'
theme.notification_opacity = 1

theme.border_normal = '#20253e'
theme.border_focus = '#545277'
theme.border_marked = '#424760'
theme.border_width  = dpi(2)

theme.hud_popup_bg = theme.bg_normal
theme.hud_popup_border_color = theme.border_focus
theme.hud_popup_border_width = theme.border_width
theme.hud_slider_fg = theme.clr.blue
theme.hud_slider_bg = theme.clr.gray

theme.tasklist_plain_task_name = true
theme.tasklist_disable_icon = true
theme.useless_gap = dpi(5)
theme.gap_single_client = true

-- Assets
-- theme.titlebar_close_button_normal = theme_path.."assets/buttons/close-button.svg"
-- theme.titlebar_close_button_focus = theme_path.."assets/buttons/close-button.svg"
--
-- theme.titlebar_minimize_button_normal = theme_path.."assets/buttons/minimize-button.svg"
-- theme.titlebar_minimize_button_focus = theme_path.."assets/buttons/minimize-button.svg"
--
-- theme.titlebar_maximized_button_normal = theme_path.."assets/buttons/maximized-button.svg"
-- theme.titlebar_maximized_button_focus = theme_path.."assets/buttons/maximized-button.svg"


theme.titlebar_close_button_normal = theme_path.."assets/titlebar/close_normal.png"
theme.titlebar_close_button_focus  = theme_path.."assets/titlebar/close_focus.png"

theme.titlebar_minimize_button_normal = theme_path.."assets/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus  = theme_path.."assets/titlebar/minimize_focus.png"

theme.titlebar_ontop_button_normal_inactive = theme_path.."assets/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive  = theme_path.."assets/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = theme_path.."assets/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active  = theme_path.."assets/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = theme_path.."assets/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive  = theme_path.."assets/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = theme_path.."assets/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active  = theme_path.."assets/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = theme_path.."assets/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive  = theme_path.."assets/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = theme_path.."assets/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active  = theme_path.."assets/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = theme_path.."assets/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = theme_path.."assets/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active = theme_path.."assets/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active  = theme_path.."assets/titlebar/maximized_focus_active.png"

theme.layout_fairh = theme_path.."assets/layouts/fairhw.png"
theme.layout_fairv = theme_path.."assets/layouts/fairvw.png"
theme.layout_floating  = theme_path.."assets/layouts/floatingw.png"
theme.layout_magnifier = theme_path.."assets/layouts/magnifierw.png"
theme.layout_max = theme_path.."assets/layouts/maxw.png"
theme.layout_fullscreen = theme_path.."assets/layouts/fullscreenw.png"
theme.layout_tilebottom = theme_path.."assets/layouts/tilebottomw.png"
theme.layout_tileleft   = theme_path.."assets/layouts/tileleftw.png"
theme.layout_tile = theme_path.."assets/layouts/tilew.png"
theme.layout_tiletop = theme_path.."assets/layouts/tiletopw.png"
theme.layout_spiral  = theme_path.."assets/layouts/spiralw.png"
theme.layout_dwindle = theme_path.."assets/layouts/dwindlew.png"
theme.layout_cornernw = theme_path.."assets/layouts/cornernww.png"
theme.layout_cornerne = theme_path.."assets/layouts/cornernew.png"
theme.layout_cornersw = theme_path.."assets/layouts/cornersww.png"
theme.layout_cornerse = theme_path.."assets/layouts/cornersew.png"

-- Notifications
theme.notification_border_color = theme.bg_light
theme.notification_border_width = dpi(3)

naughty.config.padding = dpi(8)
naughty.config.defaults = {
    timeout = 5,
    text = "",
    ontop = true,
    position = "top_right",
    margin = dpi(10),
}


-- Generate taglist squares:
local taglist_square_size = dpi(4)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
    taglist_square_size, theme.fg_normal
)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
    taglist_square_size, theme.fg_normal
)

theme.menu_submenu_icon = theme_path.."submenu.png"
theme.menu_height = dpi(15)
theme.menu_width  = dpi(100)

-- Generate Awesome icon:
theme.awesome_icon = theme_assets.awesome_icon(
    theme.menu_height, theme.bg_focus, theme.fg_focus
)

theme.icon_theme = nil

return theme
