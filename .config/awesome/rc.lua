-------------------------------------------------------------------------
--                             DEPENDENCIES                            --
-------------------------------------------------------------------------

-- awesome-wm-widgets (https://github.com/streetturtle/awesome-wm-widgets)
-- alsa-utils (https://archlinux.org/packages/?name=alsa-utils)
-- pamixer (https://archlinux.org/packages/community/x86_64/pamixer/)
-- acpilight (https://archlinux.org/packages/?name=acpilight)
-- betterlockscreen (https://github.com/betterlockscreen/betterlockscreen)
-- dmenu (https://wiki.archlinux.org/title/Dmenu)
-- Arc icon theme (https://archlinux.org/packages/community/any/arc-icon-theme/)
-- Iosevka Nerd Font (https://archlinux.org/packages/community/any/ttf-iosevka-nerd)
-- FiraMono Nerd Font (https://aur.archlinux.org/packages/otf-nerd-fonts-fira-mono)
-- polkit-gnome (https://archlinux.org/packages/?name=polkit-gnome)
-- picom (https://wiki.archlinux.org/title/Picom)
-- ranger (https://wiki.archlinux.org/title/Ranger)
-- firefox (https://wiki.archlinux.org/title/Firefox)
-- alacritty (https://wiki.archlinux.org/title/Alacritty)

-------------------------------------------------------------------------
--                               IMPORTS                               --
-------------------------------------------------------------------------

pcall(require, "luarocks.loader")

local gears = require("gears")
local awful = require("awful")
local beautiful = require("beautiful")
local naughty = require("naughty")
local menubar = require("menubar")
--local sharedtags = require("sharedtags")

require("awful.autofocus")
require("awful.hotkeys_popup.keys")

-------------------------------------------------------------------------
--                           ERROR HANDLING                            --
-------------------------------------------------------------------------

-- Load default config if encountered errors
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true
        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end

-------------------------------------------------------------------------
--                                SETUP                                --
-------------------------------------------------------------------------

TERMINAL = "alacritty"
BROWSER = "firefox"
LAUNCHER = "rofi -show drun -show-icons"
TAGS = { "", "", "", "", "", ""}
MODKEY = "Mod4"

awful.util.tagnames = TAGS

awful.layout.layouts = {
    awful.layout.suit.tile,
    awful.layout.suit.floating,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.top,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.max,
    awful.layout.suit.max.fullscreen,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    -- awful.layout.suit.spiral,
    -- awful.layout.suit.spiral.dwindle,
    -- awful.layout.suit.magnifier,
    -- awful.layout.suit.corner.nw,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
}

-- Set the terminal for applications that require it
menubar.utils.terminal = TERMINAL

local themes = {
    "one-dark"
}

beautiful.init(gears.filesystem.get_configuration_dir().."themes/"..themes[1].."/theme.lua")

require("config/keys")
require("config/rules")
require("config/signals")
require("components/rounded_panel")
require("components/popup_slider")

-------------------------------------------------------------------------
--                               AUTOSTART                             --
-------------------------------------------------------------------------

local apps = {
    "picom",
    "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1",
    "emacs --daemon",
    "nm-applet",
    "qbittorrent",
    "nextcloud"
}

for _, app in ipairs(apps) do
    awful.spawn.single_instance(app, awful.rules.rules)
end
