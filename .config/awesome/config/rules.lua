-------------------------------------------------------------------------
--                               IMPORTS                               --
-------------------------------------------------------------------------

local awful = require("awful")
local beautiful = require("beautiful")

-------------------------------------------------------------------------
--                             WINDOW RULES                            --
-------------------------------------------------------------------------

awful.rules.rules = {
    -- All clients will match this rule.
    {
        rule = { },
        properties = {
            border_width = beautiful.border_width,
            border_color = beautiful.border_normal,
            focus = awful.client.focus.filter,
            raise = true,
            keys = windows_keys,
            buttons = windows_buttons,
            screen = awful.screen.preferred,
            placement = awful.placement.centered+awful.placement.no_overlap+awful.placement.no_offscreen
        }
    },

    -- Floating clients.
    {
        rule_any = {
            instance = {
                "DTA",    -- Firefox addon DownThemAll.
                "copyq",  -- Includes session name in class.
                "pinentry",
            },
            class = {
                "Arandr",
                "Pavucontrol",
                "Nextcloud",
                "Blueman-manager",
                "Gpick",
                "Kruler",
                "MessageWin",  -- kalarm.
                "Sxiv",
                "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
                "Wpa_gui",
                "veromix",
                "xtightvncviewer"
            },
            name = {
                "Event Tester",  -- xev.
            },
            role = {
                "AlarmWindow",    -- Thunderbird's calendar.
                "ConfigManager",  -- Thunderbird's about:config.
                "pop-up",         -- e.g. Google Chrome's (detached) Developer Tools.
            }
      },
      properties = { floating = true }
    },

    -- Disable titlebars for normal clients
    {
        rule_any = { type = { "normal", "dialog" } },
        properties = { titlebars_enabled = false }
    },

    -- Program rules
    {
        rule = { class = "firefox" },
        properties = { tag = TAGS[1] }
    },
    {
        rule = { class = "Emacs" },
        properties = { tag = TAGS[2] }
    },
    {
        rule = { class = "Steam" },
        properties = { tag = TAGS[3] }
    },
    {
        rule = { class = "Lutris" },
        properties = { tag = TAGS[3] }
    },
    {
        rule = { class = "youtube-music-desktop-app" },
        properties = { tag = TAGS[4] }
    },
    {
        rule = { class = "YouTube Music" },
        properties = { tag = TAGS[4] }
    },
    {
        rule = { class = "Caprine" },
        properties = { tag = TAGS[5] }
    },
    {
        rule = { class = "Slack" },
        properties = { tag = TAGS[5] }
    },
    {
        rule = { class = "discord" },
        properties = { tag = TAGS[5] }
    },
    {
        rule = { class = "qBittorrent" },
        properties = { tag = TAGS[6] }
    },
}
