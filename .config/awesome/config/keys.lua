-------------------------------------------------------------------------
--                               IMPORTS                               --
-------------------------------------------------------------------------

local gears = require("gears")
local awful = require("awful")
local beautiful = require("beautiful")
local hotkeys_popup = require("awful.hotkeys_popup")

--local brightness_widget = require("awesome-wm-widgets.brightness-widget.brightness")
local volume_widget = require('awesome-wm-widgets.volume-widget.volume')
local logout_popup = require("awesome-wm-widgets.logout-popup-widget.logout-popup")

-------------------------------------------------------------------------
--                                 KEYS                                --
-------------------------------------------------------------------------

-- Global keybindings
global_keys = gears.table.join(

    -- Programs
    awful.key({ MODKEY }, "Return", function() awful.spawn(TERMINAL) end,
        { description = "terminal", group = "Programs" }),
    awful.key({ MODKEY }, "r", function() awful.spawn("emacsclient -nc") end,
        { description = "emacs", group = "Programs" }),
    awful.key({ MODKEY }, "d", function() awful.util.spawn(LAUNCHER) end,
        { description = "dmenu", group = "Programs" }),
    awful.key({ MODKEY }, "w", function() awful.util.spawn(BROWSER) end,
        { description = "browser", group = "Programs" }),
    awful.key({ MODKEY }, "e", function() awful.util.spawn(TERMINAL .. " -e ranger") end,
        { description = "file manager", group = "Programs" }),
    awful.key({ MODKEY, "Shift" }, "s", function() awful.util.spawn("flameshot gui") end,
        { description = "take a screenshot", group = "Programs" }),
    awful.key({ "Control", "Mod1" }, "l", function() awful.util.spawn("betterlockscreen -l") end,
        { description = "lock screen", group = "Programs" }),
    awful.key({ MODKEY, "Shift" }, "n", function() awful.util.spawn(TERMINAL .. " -e nmtui connect") end,
        { description = "lock screen", group = "Programs" }),

    -- Awesome
    awful.key({ MODKEY }, "s", hotkeys_popup.show_help,
        { description="show cheatsheet", group="Awesome" }),
    awful.key({ MODKEY, "Shift" }, "r", awesome.restart,
        { description = "reload awesome", group = "Awesome" }),
    awful.key({ MODKEY, "Shift" }, "c", awesome.quit,
        { description = "quit awesome", group = "Awesome" }),

    awful.key({ MODKEY }, "=",
        function()
            local systray = nil
            for s in screen do
                systray = s.systray
                systray.visible = not systray.visible
            end
            systray.widget:set_screen(awful.screen.focused())
        end,
        { description="show systray", group="Awesome" }),

    -- TODO Make it so it actually reads .xprofile
    awful.key({ MODKEY, "Shift" }, "f",
        function()
            awful.spawn("setxkbmap pl")
            awful.spawn("setxkbmap -option caps:swapescape")
            awful.spawn("xset r rate 300 50")
            awful.spawn("xset -b")
            awful.spawn("xset -dpms")
            awful.spawn("setterm -blank 0 -powerdown 0")
            awful.spawn("xset s off")
            awful.spawn("autorandr")
            awful.spawn("autorandr --change")
            awful.spawn("xrandr --output HDMI-1-0 --primary")
        end,
        { description = "reload xprofile", group = "Awesome" }),

    awful.key({ MODKEY }, "x",
        function()
            logout_popup.launch({
                onlock = function() awful.spawn("betterlockscreen -l") end,
                accent_color = beautiful.border_focus
            })
        end,
        { description = "show logout screen", group = "Awesome" }),

    -- Screens
    awful.key({ MODKEY }, "z", function() awful.screen.focus_relative(1) end,
        { description = "focus the next screen", group = "Screen" }),
    awful.key({ MODKEY, "Control" }, "z", function() awful.screen.focus_relative(-1) end,
        { description = "focus the previous screen", group = "Screen" }),

    -- Layouts
    awful.key({ MODKEY }, "l", function() awful.tag.incmwfact( 0.05) end,
        { description = "increase master width", group = "Layouts" }),
    awful.key({ MODKEY }, "h", function() awful.tag.incmwfact(-0.05) end,
        { description = "decrease master width", group = "Layouts" }),
    awful.key({ MODKEY, "Shift" }, "h", function() awful.tag.incnmaster(1, nil, true) end,
        { description = "increase the number of master windows", group = "Layouts" }),
    awful.key({ MODKEY, "Shift" }, "l", function() awful.tag.incnmaster(-1, nil, true) end,
        { description = "decrease the number of master windows", group = "Layouts" }),
    awful.key({ MODKEY, "Control" }, "h", function() awful.tag.incncol(1, nil, true) end,
        { description = "increase the number of columns", group = "Layouts" }),
    awful.key({ MODKEY, "Control" }, "l", function() awful.tag.incncol(-1, nil, true) end,
        { description = "decrease the number of columns", group = "Layouts" }),
    awful.key({ MODKEY }, "space", function () awful.layout.inc(1) end,
        { description = "select next layout", group = "Layouts" }),
    awful.key({ MODKEY, "Shift" }, "space", function () awful.layout.inc(-1) end,
        { description = "select previous layout", group = "Layouts" }),

    -- Window management
    awful.key({ MODKEY }, "j", function() awful.client.focus.byidx(1) end,
        { description = "focus next window", group = "Windows" }),
    awful.key({ MODKEY }, "k", function() awful.client.focus.byidx(-1) end,
        { description = "focus previous window", group = "Windows" }),
    awful.key({ MODKEY, "Shift" }, "j", function() awful.client.swap.byidx(1) end,
        { description = "swap with next window", group = "Windows" }),
    awful.key({ MODKEY, "Shift" }, "k", function() awful.client.swap.byidx(-1) end,
        { description = "swap with previous window", group = "Windows" }),
    awful.key({ MODKEY }, "u", awful.client.urgent.jumpto,
        { description = "jump to urgent client", group = "Windows" }),
    awful.key({ MODKEY }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end,
        { description = "Alt Tab", group = "Windows" }
    ),
    awful.key({ MODKEY, "Control" }, "n",
        function()
            local c = awful.client.restore()
            -- Focus restored client
            if c then
                c:emit_signal(
                    "request::activate",
                    "key.unminimize",
                    {raise = true}
                )
            end
        end,
        { description = "restore minimized", group = "Windows" }
    ),

    -- Fn keys
    awful.key({ }, "XF86AudioRaiseVolume",
        function()
            volume_widget:inc(5)
            awesome.emit_signal("volume_change")
            --awful.spawn.easy_async("pamixer -i 2", function() awesome.emit_signal("volume_change") end)
        end,
        { description = "increase volume", group = "Fn" }),
    awful.key({ }, "XF86AudioLowerVolume",
        function()
            volume_widget:dec(5)
            awesome.emit_signal("volume_change")
            --awful.spawn.easy_async("pamixer -d 2", function() awesome.emit_signal("volume_change") end)
        end,
        { description = "decrease volume", group = "Fn" }),
    awful.key({ }, "XF86AudioMute",
        function()
            awful.spawn.easy_async("pamixer -t", function() awesome.emit_signal("volume_change") end)
        end,
        { description = "mute volume", group = "Fn" }),
    awful.key({ }, "XF86MonBrightnessUp",
        function()
            awful.spawn.easy_async("xbacklight -inc 5", function() awesome.emit_signal("brightness_change") end)
        end,
        { description = "increase brightness", group = "Fn" }),
    awful.key({ }, "XF86MonBrightnessDown",
        function ()
            awful.spawn.easy_async("xbacklight -dec 5", function() awesome.emit_signal("brightness_change") end)
        end,
        { description = "decrease brightness", group = "Fn" })
)

-- Window keybindings
windows_keys = gears.table.join(
    awful.key({ MODKEY, "Shift" }, "q", function(c) c:kill() end,
        { description = "close window", group = "Windows" }),
    awful.key({ MODKEY, "Shift" }, "Return", function(c) c:swap(awful.client.getmaster()) end,
        { description = "move to master", group = "Windows" }),
    awful.key({ MODKEY, "Shift" }, "z", function(c) c:move_to_screen() end,
        { description = "move to screen", group = "Windows" }),
    awful.key({ MODKEY, "Control" }, "space",  awful.client.floating.toggle,
        { description = "toggle floating", group = "Windows" }),
    awful.key({ MODKEY }, "n", function(c) c.minimized = true end,
        { description = "minimize", group = "Windows" }),
    awful.key({ MODKEY }, "f",
        function (c)
            c.maximized = not c.maximized
            c:raise()
        end ,
        { description = "(un)maximize", group = "Windows" }
    ),
    awful.key({ MODKEY }, "m",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        { description = "toggle fullscreen", group = "Windows" }
    )
)

-- Tags keybindings
for i = 1, #TAGS do
    global_keys = gears.table.join(global_keys,

        awful.key({ MODKEY }, "#" .. i + 9,
            function ()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then
                    tag:view_only()
                end
            end,
            { description = "view tag #"..i, group = "Tags" }
        ),

        awful.key({ MODKEY, "Control", "Shift" }, "#" .. i + 9,
            function ()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then
                   awful.tag.viewtoggle(tag)
                end
            end,
            { description = "toggle tag #" .. i, group = "Tags" }
        ),

        awful.key({ MODKEY, "Shift" }, "#" .. i + 9,
            function ()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:move_to_tag(tag)
                    end
               end
            end,
            { description = "move focused client to tag #"..i, group = "Tags" }
        ),

        awful.key({ MODKEY, "Control" }, "#" .. i + 9,
            function ()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:toggle_tag(tag)
                    end
                end
            end,
            { description = "toggle focused client on tag #" .. i, group = "Tags" }
        )
    )
end

-- Mouse buttons
windows_buttons = gears.table.join(
    awful.button({ }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
    end),
    awful.button({ MODKEY }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.move(c)
    end),
    awful.button({ MODKEY }, 3, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.resize(c)
    end)
)

-- Set global key bindings
root.keys(global_keys)

-- Set global mouse bindings
--root.buttons(gears.table.join(
      -- Cycle through workspace with scroll wheel
--    awful.button({ }, 4, awful.tag.viewnext),
--    awful.button({ }, 5, awful.tag.viewprev)
--))
