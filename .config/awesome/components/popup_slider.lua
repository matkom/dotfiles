-------------------------------------------------------------------------
--                               IMPORTS                               --
-------------------------------------------------------------------------

local wibox = require("wibox")
local awful = require("awful")
local gears = require("gears")
local beautiful = require("beautiful")
local dpi = beautiful.xresources.apply_dpi

local icon_dir = gears.filesystem.get_configuration_dir() .. "/icons/"

-------------------------------------------------------------------------
--                                DRIP                                 --
-------------------------------------------------------------------------

local popup_icon = wibox.widget {
   widget = wibox.widget.imagebox,
   forced_height = dpi(26),
   forced_width = dpi(26),
}

local popup_slider = wibox({
   screen = awful.screen.focused(),
   width = dpi(256),
   height = dpi(56),
   bg = beautiful.hud_popup_bg,
   shape = gears.shape.rounded_bar,
   border_width = beautiful.hud_popup_border_width,
   border_color = beautiful.hud_popup_border_color,
   visible = false,
   ontop = true
})

local popup_progressbar = wibox.widget{
   widget = wibox.widget.progressbar,
   shape = gears.shape.rounded_bar,
   color = beautiful.hud_slider_fg,
   background_color = beautiful.hud_slider_bg,
   max_value = 100,
   value = 0
}

popup_slider:setup {
    wibox.container.margin(
       popup_icon, dpi(20), dpi(0), dpi(14), dpi(0)
    ),
   {
      wibox.container.margin(
         popup_progressbar, dpi(20), dpi(20), dpi(20), dpi(20)
      ),
      direction = "east",
      layout = wibox.layout.align.horizontal,
   },
   layout = wibox.layout.align.horizontal,
}

local hide_popup = gears.timer {
   timeout = 4,
   autostart = true,
   callback = function()
      popup_slider.visible = false
   end
}

local function refresh_popup_pos()
    popup_slider.screen = awful.screen.focused()
    popup_slider.x = popup_slider.screen.geometry.x
        + (popup_slider.screen.geometry.width / 2)
        - (popup_slider.width / 2)
    popup_slider.y = popup_slider.screen.geometry.y
        + (popup_slider.screen.geometry.height)
        - (popup_slider.height * 4)
end

awesome.connect_signal("volume_change",
    function()
      awful.spawn.easy_async_with_shell("pamixer --get-volume && pamixer --get-mute",
         function(stdout)
            refresh_popup_pos()
            lines = {}
            for s in stdout:gmatch("[^\n]+") do
                table.insert(lines, s)
            end

            local volume = 0

            if lines[2] ~= "true" then
                volume = tonumber(lines[1])
            end

            if (volume > 50) then
                popup_icon:set_image(icon_dir .. "volume/volume-high-white.png")
            elseif (volume > 0) then
                popup_icon:set_image(icon_dir .. "volume/volume-low-white.png")
            else
                popup_icon:set_image(icon_dir .. "volume/volume-mute-white.png")
            end

            popup_progressbar.value = volume
         end,
         false
      )

      if popup_slider.visible then
         hide_popup:again()
      else
         popup_slider.visible = true
         hide_popup:start()
      end
   end
)

awesome.connect_signal("brightness_change",
    function()
      awful.spawn.easy_async_with_shell("xbacklight -get",
         function(brightness)
            refresh_popup_pos()
            --TODO Scale image
            popup_icon:set_image(icon_dir .. "brightness-white.png")
            popup_progressbar.value = tonumber(brightness)
         end,
         false
      )

      if popup_slider.visible then
         hide_popup:again()
      else
         popup_slider.visible = true
         hide_popup:start()
      end
    end
)
