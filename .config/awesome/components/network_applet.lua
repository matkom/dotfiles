local awful = require("awful")
local wibox = require("wibox")
local gears = require("gears")
local beautiful = require("beautiful")

local nm_popup = awful.popup {
    bg = beautiful.bg_normal,
    ontop = true,
    visible = false,
    shape = gears.shape.rounded_rect,
    border_width = beautiful.border_width
}

nm_widget = wibox.widget {
    image  = beautiful.awesome_icon,
    resize = false,
    widget = wibox.widget.imagebox
}

nm_widget:buttons {
    awful.button({}, 3, function()
        if nm_popup.visible then
            nm_popup.visible = false
        else
            nm_popup.visible = true
            nm_popup.move_next_to(mouse.current_widget_geometry)
        end
    end)
}
