-------------------------------------------------------------------------
--                               IMPORTS                               --
-------------------------------------------------------------------------

local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
local dpi = require('beautiful.xresources').apply_dpi

local batteryarc_widget = require("awesome-wm-widgets.batteryarc-widget.batteryarc")
local volume_widget = require('awesome-wm-widgets.volume-widget.volume')
--local nm_widget = require("network_applet")

-------------------------------------------------------------------------
--                               WIDGETS                               --
-------------------------------------------------------------------------

local tasklist_buttons = gears.table.join(
    awful.button({ }, 1,
        function (c)
            if c == client.focus then
                c.minimized = true
            else
                c:emit_signal(
                    "request::activate",
                    "tasklist",
                    { raise = true }
                )
            end
        end
    ),
    awful.button({ }, 3, function() awful.menu.client_list({ theme = { width = 250 } }) end),
    awful.button({ }, 4, function() awful.client.focus.byidx(1) end),
    awful.button({ }, 5, function () awful.client.focus.byidx(-1) end)
)

local function my_tasklist(s)
    return awful.widget.tasklist {
        screen = s,
        filter = awful.widget.tasklist.filter.currenttags,
        buttons = tasklist_buttons,
        style = {
            shape_border_width = 1,
            shape_border_color = beautiful.border_focus,
            shape  = gears.shape.rounded_bar,
        },
        layout = {
            spacing = 10,
            spacing_widget = {
                {
                    forced_width = 5,
                    shape = gears.shape.circle,
                    widget = wibox.widget.separator
                },
                valign = 'center',
                halign = 'center',
                widget = wibox.container.place,
            },
            layout  = wibox.layout.flex.horizontal
        },
        widget_template = {
            {
                {
                    {
                        {
                            id     = 'clienticon',
                            widget = awful.widget.clienticon,
                        },
                        margins = 5,
                        widget = wibox.container.margin,
                    },
                    {
                        id = 'text_role',
                        widget = wibox.widget.textbox,
                    },
                    layout = wibox.layout.fixed.horizontal,
                },
                left  = 10,
                right = 10,
                widget = wibox.container.margin
            },
            id = 'background_role',
            widget = wibox.container.background,
            create_callback = function(self, c, index, objects) --luacheck: no unused args
                self:get_children_by_id('clienticon')[1].client = c
            end,
        }
    }
end

local taglist_buttons = gears.table.join(
    awful.button({ }, 1, function(t) t:view_only() end),
    awful.button({ MODKEY }, 1,
        function(t)
            if client.focus then
                client.focus:move_to_tag(t)
            end
        end
    ),
    awful.button({ }, 3, awful.tag.viewtoggle),
    awful.button({ MODKEY }, 3,
        function(t)
            if client.focus then
                client.focus:toggle_tag(t)
            end
        end
    ),
    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
)

local function my_taglist(s)
    return awful.widget.taglist {
        screen = s,
        filter = awful.widget.taglist.filter.all,
        widget_template = {
            {
                {
                    id = 'text_role',
                    widget = wibox.widget.textbox,
                },
                layout = wibox.layout.align.horizontal,
            },
            left = 10,
            right = 10,
            widget = wibox.container.margin,
        },
        buttons = taglist_buttons
    }
end

local function my_wibox(s)
    return awful.wibar {
        screen = s,
        position = "top",
        type = "dock",
        shape = function(cr, width, height)
            gears.shape.rounded_bar(cr, width, height)
        end,
        height = 32,
        width = 1900,
        opacity = 0.95,
        bg = beautiful.bg_normal,
        border_width = 1,
        border_color = beautiful.border_focus,
        visible = true,
        ontop = false,
        expand = true,
    }
end

local function set_wallpaper(s)
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end

local my_textclock = wibox.layout.margin(
    wibox.widget.textclock(
        '<span font="'..beautiful.font..
        '" color="'..beautiful.fg_focus..
        '">%a %b %d %H:%M:%S </span>',
        1
    ),
    0, 5, 5, 5
)

screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(s)
    set_wallpaper(s)

    awful.tag(TAGS, s, awful.layout.layouts[1])

    s.mypromptbox = awful.widget.prompt()

    s.mylayoutbox = wibox.container.margin(
        awful.widget.layoutbox(s), dpi(3), dpi(3), dpi(3), dpi(3))

    s.mylayoutbox:buttons(gears.table.join(
            awful.button({ }, 1, function () awful.layout.inc( 1) end),
            awful.button({ }, 3, function () awful.layout.inc(-1) end),
            awful.button({ }, 4, function () awful.layout.inc( 1) end),
            awful.button({ }, 5, function () awful.layout.inc(-1) end)
    ))

    s.mytaglist = my_taglist(s)
    s.mytasklist = my_tasklist(s)

    s.systray = wibox.layout.margin(wibox.widget.systray(), 5, 5, 5, 5)

    s.mywibox = my_wibox(s)
    awful.placement.top(s.mywibox, { margins = 5 })

    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        {
            layout = wibox.layout.fixed.horizontal,
            spacing = 5,
            s.mytaglist,
            s.mylayoutbox,
            s.mypromptbox,
        },
        s.mytasklist,
        {
            layout = wibox.layout.fixed.horizontal,
            wibox.widget.textbox(" "),
            {
                layout = wibox.layout.fixed.horizontal,
                spacing = 10,
                --nm_widget,
                s.systray,
                batteryarc_widget {
                    show_current_level = true,
                    arc_thickness = 1,
                    notification_position = "top_right",
                    show_notification_mode = "on_hover"
                },
                volume_widget { widget_type = "arc" },
                my_textclock
            }
        },
    }
end)
