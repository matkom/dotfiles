;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                MY CONFIG                                   ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Load PATH
(when (daemonp)
  (exec-path-from-shell-initialize))

;; Tell emacs that I have other files in config too lol
(add-to-list 'load-path "~/.config/doom/functions/")

;; Remove retarded hook that prevents me from replacing characters with <s> key
(remove-hook 'doom-first-input-hook #'evil-snipe-mode)

;;doom-font (font-spec :family "Nerd Font" :size 12)

;; Directory where projects are located
(setq projectile-project-search-path '("~/Projects/"))

;; Minimap
(setq minimap-window-location 'right)
(map! :leader
      (:prefix ("o" . "open")
       :desc "Open minimap" "m" #'minimap-mode))

;; Org mode settings
(after! org
  (setq org-startup-folded t)
  (setq org-directory "~/Documents/Org/")
  (setq org-agenda-files '("~/Documents/Org/Todos.org" "~/Documents/Org/Events.org"))
  (setq org-publish-project-alist
        '(("org-files"
           :base-directory "~/Projects/blog"
           :base-extension "org"
           :publishing-directory "~/Projects/blog/public/"
           :resursive t
           :publishing-function org-html-publish-to-html)
          ("static-files"
           :base-directory "~/Projects/blog"
           :base-extension "css\\|png\\|jpg"
           :publishing-directory "~/Projects/blog/public/"
           :recursive t
           :publishing-function org-publish-attachment)
          ("blog"
           :components ("org-files" "static-files"))))
  ;;(setq org-todo-keywords '((sequence "TODO(t)" "DONE(d)" "NEXT(n)" "PROJ(p)")))
)

;; Debugger
(setq dap-auto-configure-mode t)
(dap-ui-mode 1)
(dap-tooltip-mode 1)
(tooltip-mode 1)(require 'dap-lldb)
(require 'dap-gdb-lldb)

;; Moving buffers
(require 'buffer-move)
(global-set-key (kbd "<C-S-k>") 'buf-move-up)
(global-set-key (kbd "<C-S-j>") 'buf-move-down)
(global-set-key (kbd "<C-S-h>") 'buf-move-left)
(global-set-key (kbd "<C-S-l>") 'buf-move-right)

;; Dired settings
(setq dired-recursive-deletes 'always)
(setq dired-recursive-copies 'always)

;; Debugger
(setq dap-lldb-debug-program '("/usr/bin/lldb-vscode"))

(defun dap-debug-create-or-edit-json-template ()
  (interactive)
  (let ((filename (concat (lsp-workspace-root) "/launch.json"))
        (default "~/.emacs.d/default-launch.json"))
    (unless (file-exists-p filename)
      (copy-file default filename))
    (find-file-existing filename)))

(map! :map dap-mode-map
      :leader
      :prefix ("d" . "dap")
      ;; basics
      :desc "dap next"          "n" #'dap-next
      :desc "dap step in"       "i" #'dap-step-in
      :desc "dap step out"      "o" #'dap-step-out
      :desc "dap continue"      "c" #'dap-continue
      :desc "dap hydra"         "h" #'dap-hydra
      :desc "dap debug restart" "r" #'dap-debug-restart
      :desc "dap debug"         "s" #'dap-debug

      ;; debug
      :prefix ("dd" . "Debug")
      :desc "dap debug recent"  "r" #'dap-debug-recent
      :desc "dap debug last"    "l" #'dap-debug-last

      ;; eval
      :prefix ("de" . "Eval")
      :desc "eval"                "e" #'dap-eval
      :desc "eval region"         "r" #'dap-eval-region
      :desc "eval thing at point" "s" #'dap-eval-thing-at-point
      :desc "add expression"      "a" #'dap-ui-expressions-add
      :desc "remove expression"   "d" #'dap-ui-expressions-remove

      :prefix ("db" . "Breakpoint")
      :desc "dap breakpoint toggle"      "b" #'dap-breakpoint-toggle
      :desc "dap breakpoint condition"   "c" #'dap-breakpoint-condition
      :desc "dap breakpoint hit count"   "h" #'dap-breakpoint-hit-condition
      :desc "dap breakpoint log message" "l" #'dap-breakpoint-log-message)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                              DOOM DEFAULTS                                 ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "MK"
      user-mail-address "noemailforyou@spy.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-one)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type 'relative)

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
