#!/bin/sh                                                                                                                                                                                       

# Automatically setup external monitor
# Place this script inside /usr/bin so udev rule can run it

xrandr_command="/usr/bin/xrandr"
sed_command="/bin/sed"

is_hdmi_connected=`$xrandr_command | $sed_command -n '/HDMI-1-0 connected/p'`

if [ -n "$is_hdmi_connected" ]; then
  xrandr --output HDMI-1-0 --auto --right-of eDP-1
else
  $xrandr_command --output HDMI-1-0 --off
fi
