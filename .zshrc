######################################################################################
# ENVIRONMENT
######################################################################################

# Home cleanup
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
#export RUSTUP_HOME="$XDG_DATA_HOME/rustup"
#export CARGO_HOME="$XDG_DATA_HOME/cargo"

export HISTFILE="${XDG_STATE_HOME}"/bash/history
export CUDA_CACHE_PATH="$XDG_CACHE_HOME"/nv
export GNUPGHOME="$XDG_DATA_HOME"/gnupg

# Preferred editor
export EDITOR="vim"
export VISUAL="emacsclient -nc"

# For fuzzy finder to show hidden files
#export FZF_DEFAULT_COMMAND="find ."

# Add programs to PATH
export PATH="$PATH:$HOME/.local/bin:$HOME/.config/emacs/bin:$HOME/.dotnet/tools" #:$XDG_DATA_HOME/cargo/bin"

# Cargo env
# source $XDG_DATA_HOME/cargo/env

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

######################################################################################
# ZSH
######################################################################################

# AUR
#export ZSH=/usr/share/oh-my-zsh/

# Manual installation
export ZSH="$HOME/.oh-my-zsh"

# Plugins
plugins=(
    git
    vi-mode 
    zsh-autosuggestions
    zsh-syntax-highlighting
)

# Theme
ZSH_THEME="powerlevel10k/powerlevel10k"

# Command execution time stamp shown in the history command output
HIST_STAMPS="dd.mm.yyyy"

# Controls whether the cursor style is changed when switching vi modes
VI_MODE_SET_CURSOR=true

source $ZSH/oh-my-zsh.sh
# source $ZSH/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

######################################################################################
# ALIASES
######################################################################################

# GRUB
alias update-grub="sudo grub2-mkconfig -o /boot/grub2/grub.cfg"

# Optimus manager
alias ompm="optimus-manager --print-mode"
alias omsi="optimus-manager --switch integrated"
alias omsd="optimus-manager --switch nvidia"
alias omsh="optimus-manager --switch hybrid"
# Check whether NVIDA GPU is up
alias nvstat="cat /sys/bus/pci/devices/0000:01:00.0/power/runtime_status"
# Check what GPU is being used
alias gpu='inxi -SGx'

# Systemcl
alias sstat='sudo systemctl status'
alias senable='sudo systemctl enable'
alias sstop'sudo systemctl stop'
alias sstart='sudo systemctl start'
alias srestart='sudo systemctl restart'

# Reload touchpad module
alias fix-touchpad='modprobe -r i2c_hid_acpi && modprobe i2c_hid_acpi'
# Reload bluetooth module
alias fix-bluetooth='sudo usb_modeswitch -R -v 0bda -p 4852'

# Timeshift
alias tl='sudo timeshift --list'
alias tc='sudo timeshift --create --comments MANUAL_$(date +"%d-%m-%Y_%T")'
alias tr='sudo timeshift --delete --snapshot'

# TODO Finish
# Remove manual timeshift snapshots
#snap_name='$(sudo timeshift --list | grep MANUAL)'
#echo ${snap_name}
#alias tr='sudo timeshift --delete --snapshot ${snap_name}'

# Night light
alias night-on='redshift -P -O 4000'
alias night-off='redshift -x'

# Get program class name
alias xpropp='xprop | grep CLASS'

# Tmux
alias tx='tmux'

# Ranger
alias r='ranger'

# Mock text
alias mock='python $HOME/Scripts/mock.py'

# Chec what is run using nvidia graphics card
alias nvidia-check='watch -n 1 nvidia-smi'

# Timer
alias tm='$HOME/Scripts/timer.sh'

# Git
alias ga='git add'
alias gc='git commit'
alias gp='git push'
alias gs='git status'
alias gd='git diff'
alias gr='git reset'
alias gl='git log'

# My backups
alias gdr='$HOME/Scripts/backup-drive-rclone.sh'
alias bp='$HOME/Scripts/backup-plasma.sh'

# Protects against stupid mistakes :)
alias rm='rm -I'

# List
alias ls='ls --color=auto'
alias la='ls -A'
alias ll='ls -lAh'
alias l='ls'
alias l.="ls -A | egrep '^\.'"
alias lg='ls | grep'
alias llg='ls -lAh | grep'
alias lag='ls -A | grep'

# Package management
alias yas='yay -S'
alias yasu='yay -Syu'
alias yar='yay -Rsn'
alias yaq='yay -Qi'
alias yaqi='yay -Q --info'
alias yac='yay -Yc'
alias yaf='yay -Fy'

# Programs
alias v='nvim'
alias weather='curl wttr.in'

# Edit important configuration files
alias vz="$EDITOR $HOME/.zshrc"
alias vv="$EDITOR $HOME/.config/nvim/init.vim"
alias va="$EDITOR $HOME/.config/alacritty/alacritty.yml"
alias vx="$EDITOR $HOME/.xmonad/xmonad.hs"
alias vxb="$EDITOR $HOME/.config/xmobar/xmobarrc"
alias vtx="$EDITOR $HOME/.config/tmux/tmux.conf"

# Shutdown or reboot
alias sd="shutdown now"
alias rb="reboot"

# Dotfiles bare git repo
alias dot="/usr/bin/git --git-dir=$HOME/.dotfiles.git/ --work-tree=$HOME"

# Colorize the grep command output for ease of use (good for log files)
alias grep='grep --color=auto'

# Readable output for df
alias df='df -h'

# Get the error messages from journalctl
alias jctle="journalctl -p 3 -xb"

# Take a snapshot out of web cam
alias camshot='mpv av://v4l2:/dev/video0 --profile=low-latency --untimed'

# Extractor for all kinds of archives
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   tar xf $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

######################################################################################
# THINGS THAT NEED TO BE AS LOW AS POSSIBLE
######################################################################################

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
